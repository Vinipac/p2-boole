#include "rpn2tv.h"

// NOTA DE USO: PARA O PROGRAMAR FUNCIONAR, ELE PRIMEIRO DEVE SER ABERTO E ENTO DEVE-SE
// DIGITAR " '(a&b)|(c&d);' | shunt " (OU OUTRAS ENTRADAS) PARA ELE FUNCIOANR.
// Se o echo for utilizado ele colocar 4 variaveis a mais ('e', 'c', 'h', 'o') e o programa
// nao ira funcionar como desejado

// Esse programa aceita qualquer subconjunto do alfabeto.

//Funções

void leVariaveis(char vet[MAX],int *tamVari,int *numVari){
	int x, aux, teste;

	for(aux=0;aux<MAX;aux++){
		vet[aux]=0;
	}

	*numVari=0;
	*tamVari=0;
	x = getchar();
	while (x != 59){ // ASCII: ";"=59
		if ( ((x>=97) && (x<=122)) || ((x>=65)&&(x<=90)) ) { //Se X for uma letra do alfabeto - ASCII: "A"=65, "Z"=90, "a"=97, "z"=122.
			vet[*tamVari] = x;
			(*tamVari)++;
      teste++;
			(*numVari)++;

		}
		if ((x==38)||(x==124)||(x==126)){	// ASCII: "&"=38, "|"=124, "~"=126
			vet[*tamVari] = x;
			(*tamVari)++;

		}
		x = getchar();

	}


}

void addStack(struct stack_t *stack, int n){
  stack->topo++;

  stack->val[stack->topo]=n;
}

void initStack(struct stack_t *stack){
  int cont;

  for (cont=0;cont<MAX;cont++){
      stack->val[cont]=0;
  }
  stack->topo=-1; //-1 existe para a funcao addstack nao se adiantar.
}

int takeStack(struct stack_t *stack){
  int aux;

  aux = stack->val[stack->topo];
  stack->topo--;

  return aux;
}


int calcRes(int mat[MAX][MAX], char vetEntr[MAX], int tamEntr, int lin, int vetRes[MAX]){
  stack_t *stack; // Stack (pilha) usada para fazer as operações
  int contEntr; // Conta os valores do vetor entrada.
  int contVar; // Conta a coluna do valor na matriz.
  int valorEntr; //Variavel para deixar o codigo mais legivel
  int x, y; // Variaveis para guardar os valores para as operações.

  contVar=0;

  stack = malloc(sizeof(stack_t));
  initStack(stack);

  // Olha todos os vetores da entrada. Se for uma variavel (letra) jogo o numero da matriz
  // da coluna da variavel e da linha da vez na stack. Se for "&","|" ou "~" o algoritmo
  // retira valores da stack, opera entre eles e depois joga o resultado na stack.
  for (contEntr=0;contEntr<tamEntr;contEntr++){
    valorEntr = vetEntr[contEntr];

    // Se o valorEntr for uma letra ela é colocada na stack (pilha).
    // ASCII: "A"=65, "Z"=90, "a"=97, "z"=122.
    if ( ((valorEntr>=97) && (valorEntr<=122)) || ((valorEntr>=65)&&(valorEntr<=90)) ){
      addStack(stack, mat[lin][contVar]);
      contVar++;
    }

    // Se o valorEntr for "&" ele retira 2 valores da stack, faz sua operação
    // entre eles e depois joga o resultado na stack devolta
    if (valorEntr==38){
      x=takeStack(stack);
      y=takeStack(stack);
      addStack(stack, (x&y));

    }

    // Se o valorEntr for "|" ele retira 2 valores da stack, faz sua operação
    // entre eles e depois joga o resultado na stack de volta
    if (valorEntr==124){
      x=takeStack(stack);
      y=takeStack(stack);
      addStack(stack, (x|y));

    }

    // Se o valorEntr for "~" ele retira um valor da stack, opera sobre ele
    // e devolve pra stack.
    if (valorEntr==126){
      x=takeStack(stack);
      addStack(stack,~x);

    }

  }

  return stack->val[stack->topo];
}

void criaMatriz(int mat[MAX][MAX], int col, int lin){
  int contCol; //Qual coluna ele esta fazendo no momento.

  contCol=0; //Não é 0 para a formula dentro de criaColuna estar correta.
  for (contCol;contCol<col;contCol++){
      criaColuna(mat, contCol, col, lin);
  }

}

void criaColuna(int mat[MAX][MAX],int contCol,int col,int lin){
  int contLin, num, contNum, maxNum;

  contLin=0;
  num=0; //Numero que sera colocado na matriz
  maxNum=pow(2,(col-1-contCol)); // -1 é necessario para a conta.
  //Quantas vezes num é colocado na coluna até ele virar seu complemento
  contNum=0; //Contador de maxNum

  while(contLin<lin){
    if (contNum==maxNum) {
      if (num == 0) num=1;
      else num=0;
      contNum=0;
    }
    mat[contLin][contCol]=num;
    contLin++;
    contNum++;
  }
}

void printRes(int mat[MAX][MAX], char vetEntr[],int tamEntr,int numVari,int vetRes[]){
	int lin; //Contador

	printCabecalho(vetEntr, tamEntr);
	for (lin=0;lin<pow(2,numVari);lin++){
		printLinha(mat, numVari, lin, vetRes);
	}
}

void printCabecalho(char vetEntr[], int tamEntr){
	int cont; //Contador

	for (cont=0;cont<tamEntr;cont++){

		// Se vetEntr[cont] é uma letra (numeros sao codigos ASCII)
		if ( ((vetEntr[cont]>=97) && (vetEntr[cont]<=122)) || ((vetEntr[cont]>=65)&&(vetEntr[cont]<=90)) ){
				printf("%c", vetEntr[cont]);
		}
	}
  printf("\n");
}

void printLinha(int mat[MAX][MAX], int numVari, int lin, int vetRes[]){
	int cont;

	for (cont=0;cont<numVari;cont++){
		printf("%d", mat[lin][cont]);
	}
	printf(" %d\n", vetRes[lin]);
}


//Começo do main

void main(){
	int vetRes[MAX], mat[MAX][MAX];
	int tamEntr, numVari, contRes;
  char vetEntr[MAX];

	/*
	vetEntr é o vetor onde os dados da entrada são guardados. tamEntr é seu tamanho.
	numVari é a quantidade de variaveis de variaveis.
	mat é a tabela de verdade.
	vetRes é o vetor de resultados. contRes é seu contador.
	*/

	leVariaveis(vetEntr,&tamEntr,&numVari);
	criaMatriz(mat, numVari, pow(2,numVari)); //(mat, matCol, matLin, numVari)
	for (contRes=0;contRes<(pow(2,numVari));contRes++){
		vetRes[contRes]=calcRes(mat, vetEntr, tamEntr, contRes, vetRes);
	}
	printRes(mat, vetEntr, tamEntr, numVari, vetRes);
}
