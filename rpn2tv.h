#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define MAX 200

typedef struct stack_t{
  int val[MAX];
  int topo;
} stack_t;

int calcRes(int mat[MAX][MAX], char vetEntr[MAX], int tamEntr, int lin, int vetRes[MAX]);

void leVariaveis(char vet[MAX],int *tamVari,int *numVari);

void criaColuna(int mat[MAX][MAX],int contCol,int col,int lin);

void criaMatriz(int mat[MAX][MAX], int col, int lin);

void addStack(struct stack_t *stack, int n);

int takeStack(struct stack_t *stack);

void initStack(struct stack_t *stack);

void printRes(int mat[MAX][MAX], char vetEntr[],int tamEntr,int numVari,int vetRes[]);

void printCabecalho(char vetEntr[], int tamEntr);

void printLinha(int mat[MAX][MAX], int numVari, int lin, int vetRes[]);
